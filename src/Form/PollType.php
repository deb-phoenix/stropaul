<?php

namespace App\Form;

use App\Entity\Poll;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PollType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('question', TextType::class, [
                'label'=>'Saisir la question:'
            ])
            ->add('type')
            ->add('status', ChoiceType::class, [
                'choices' => [
                    'publié maintenant' => true,
                    'brouillon' => false,
                ],
                'expanded'=>true,
                'multiple'=>false,
                'label'=> false,
            ])
            ->add('answers', CollectionType::class, [ // permet de faire une collection d'input
                'entry_type' => AnswerType::class,
                'allow_add' => true, // permet d'ajouter des éléments input
                'by_reference' => false, // permet d'ajouter des valeurs par références à des objets (ou autre chose) existants
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Poll::class,
        ]);
    }
}
