<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* components/navbar.html.twig */
class __TwigTemplate_27c662d82c7ac4e2d154659c952e508a2d73aa400fcd709f6e12683a139f6136 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "components/navbar.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "components/navbar.html.twig"));

        // line 1
        echo "<nav>
    <div>
        <a href=\"";
        // line 3
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home");
        echo "\">
            <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:svgjs=\"http://svgjs.com/svgjs\" version=\"1.1\" width=\"20\" height=\"20\" x=\"0\" y=\"0\" viewBox=\"0 0 512 512\" style=\"enable-background:new 0 0 512 512\" xml:space=\"preserve\" class=\"\"><g xmlns=\"http://www.w3.org/2000/svg\"><path d=\"m106 180.999h345v-105c0-24.853-20.147-45-45-45h-361c-24.853 0-45 20.147-45 45v240c0 24.853 20.147 45 45 45h16v75c0 6.064 3.647 11.543 9.258 13.857 5.528 2.307 12.017 1.078 16.348-3.252l85.605-85.605h38.789v-90h-105c-8.291 0-15-6.709-15-15s6.709-15 15-15h106.518c2.272-11.153 7.231-21.237 13.865-30h-120.383c-8.291 0-15-6.709-15-15s6.709-15 15-15zm0-60h240c8.291 0 15 6.709 15 15s-6.709 15-15 15h-240c-8.291 0-15-6.709-15-15s6.709-15 15-15z\" fill=\"#ffffff\" data-original=\"#000000\" style=\"\" class=\"\"/><path d=\"m467 210.999h-181c-24.814 0-45 20.186-45 45v120c0 24.814 20.186 45 45 45h83.789l55.605 55.605c4.33 4.33 10.82 5.559 16.348 3.252 5.61-2.314 9.258-7.793 9.258-13.857v-45h16c24.814 0 45-20.186 45-45v-120c0-24.814-20.186-45-45-45zm-50.395 100.605-45 45c-5.859 5.859-15.352 5.859-21.211 0l-22.5-22.5c-5.859-5.859-5.859-15.352 0-21.211s15.352-5.859 21.211 0l11.895 11.895 34.395-34.395c5.859-5.859 15.352-5.859 21.211 0s5.859 15.352-.001 21.211z\" fill=\"#ffffff\" /></g></svg>
        </a>
    </div>


    ";
        // line 9
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 9, $this->source); })()), "user", [], "any", false, false, false, 9)) {
            echo " 
    ";
            // line 11
            echo "        <div>
            Compte:
        ";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 13, $this->source); })()), "user", [], "any", false, false, false, 13), "name", [], "any", false, false, false, 13), "html", null, true);
            echo " ·  
        <a class=\"navLink\" href=\"";
            // line 14
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logout");
            echo "\">déconnexion</a>
        </div>
    ";
        } else {
            // line 17
            echo "        <a class=\"navLink\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_login");
            echo "\">connexion</a>
    ";
        }
        // line 19
        echo "
</nav>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "components/navbar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 19,  74 => 17,  68 => 14,  64 => 13,  60 => 11,  56 => 9,  47 => 3,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<nav>
    <div>
        <a href=\"{{ path('home') }}\">
            <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:svgjs=\"http://svgjs.com/svgjs\" version=\"1.1\" width=\"20\" height=\"20\" x=\"0\" y=\"0\" viewBox=\"0 0 512 512\" style=\"enable-background:new 0 0 512 512\" xml:space=\"preserve\" class=\"\"><g xmlns=\"http://www.w3.org/2000/svg\"><path d=\"m106 180.999h345v-105c0-24.853-20.147-45-45-45h-361c-24.853 0-45 20.147-45 45v240c0 24.853 20.147 45 45 45h16v75c0 6.064 3.647 11.543 9.258 13.857 5.528 2.307 12.017 1.078 16.348-3.252l85.605-85.605h38.789v-90h-105c-8.291 0-15-6.709-15-15s6.709-15 15-15h106.518c2.272-11.153 7.231-21.237 13.865-30h-120.383c-8.291 0-15-6.709-15-15s6.709-15 15-15zm0-60h240c8.291 0 15 6.709 15 15s-6.709 15-15 15h-240c-8.291 0-15-6.709-15-15s6.709-15 15-15z\" fill=\"#ffffff\" data-original=\"#000000\" style=\"\" class=\"\"/><path d=\"m467 210.999h-181c-24.814 0-45 20.186-45 45v120c0 24.814 20.186 45 45 45h83.789l55.605 55.605c4.33 4.33 10.82 5.559 16.348 3.252 5.61-2.314 9.258-7.793 9.258-13.857v-45h16c24.814 0 45-20.186 45-45v-120c0-24.814-20.186-45-45-45zm-50.395 100.605-45 45c-5.859 5.859-15.352 5.859-21.211 0l-22.5-22.5c-5.859-5.859-5.859-15.352 0-21.211s15.352-5.859 21.211 0l11.895 11.895 34.395-34.395c5.859-5.859 15.352-5.859 21.211 0s5.859 15.352-.001 21.211z\" fill=\"#ffffff\" /></g></svg>
        </a>
    </div>


    {% if app.user %} 
    {# ou {% if is_granted('ROLE_USER') %} #}
        <div>
            Compte:
        {{ app.user.name }} ·  
        <a class=\"navLink\" href=\"{{ path('app_logout') }}\">déconnexion</a>
        </div>
    {% else %}
        <a class=\"navLink\" href=\"{{ path('app_login') }}\">connexion</a>
    {% endif %}

</nav>
", "components/navbar.html.twig", "/Users/vikki/Documents/SIMPLON/dev/tp9-stopaul3/templates/components/navbar.html.twig");
    }
}
