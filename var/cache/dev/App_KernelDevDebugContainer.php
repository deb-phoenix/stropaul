<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerGoF41KS\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerGoF41KS/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerGoF41KS.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerGoF41KS\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \ContainerGoF41KS\App_KernelDevDebugContainer([
    'container.build_hash' => 'GoF41KS',
    'container.build_id' => 'ecc317da',
    'container.build_time' => 1603104744,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerGoF41KS');
