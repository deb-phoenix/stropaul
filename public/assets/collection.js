var $collectionHolder;

// bouton ajouter une réponse
var $addAnswerButton = $('<button type="button" class="add_tag_link">+</button>');
var $newLinkLi = $('<li></li>').append($addAnswerButton);

jQuery(document).ready(function() {
    // ul qui collecte les réponses
    $collectionHolder = $('ul.answers');

    $collectionHolder.append($newLinkLi);

    $collectionHolder.data('index', $collectionHolder.find('input').length);

    $addAnswerButton.on('click', function(e) {
        addTagForm($collectionHolder, $newLinkLi);
    });
});

function addTagForm($collectionHolder, $newLinkLi) {
    var prototype = $collectionHolder.data('prototype');

    var index = $collectionHolder.data('index');

    var newForm = prototype;

    newForm = newForm.replace(/__name__/g, index);

    $collectionHolder.data('index', index + 1);

    var $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi.before($newFormLi);
}